package com.example.ordering.service;
import com.example.ordering.entity.Business1;
import com.example.ordering.entity.User1;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {



    User1 getUserByUsername(String UserName);


    void insertUser(String phone1, String name1, String pass_word, String pass_word1);


    void insertBusiness(String ShopName, String name, String Phone, String Email, String password, String Password2);

    Business1 getUserByUserName1(String userName1);

    List<Business1> queryBusinessList(int pageNum,int pageSize,String ShopName, String name, String Phone, String state);

    void updateBusiness(String Phone, String state);


}