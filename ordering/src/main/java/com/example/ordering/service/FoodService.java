package com.example.ordering.service;

import com.example.ordering.entity.Food;

import java.math.BigDecimal;
import java.util.List;

/**
 * (Food)表服务接口
 *
 * @author makejava
 * @since 2023-05-24 17:16:48
 */
public interface FoodService {
    /**
     * 新增数据
     *
     * @param food 实例对象
     * @return 实例对象
     */
    Food insert(Food food);
    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

    List<Food> findAll();
}
