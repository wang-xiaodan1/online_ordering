package com.example.ordering.service.impl;

import com.example.ordering.dao.UserFunctionDao;
import com.example.ordering.entity.User1;
import com.example.ordering.service.UserFunctionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserFunctionServiceImpl implements UserFunctionService {

    @Autowired
    private UserFunctionDao userFunctionDao;
//    定义了一个@Autowired注解的userFunctionDao属性，表示该类依赖于UserFunctionDao数据访问对象，并通过自动装配的方式进行注入

    @Override
    public String updateUserInfo(String username, String oldPassword, String newPassword, String phone) {
        // 根据用户名和旧密码查询用户信息
        User1 user = userFunctionDao.getUserByUsernameAndPassword(username, oldPassword);
        if (user == null) {
            return "the user name or password is incorrect";
        }
        // 更新用户密码和手机号
        int result = userFunctionDao.updateUserInfo(username, newPassword, phone);
        if (result > 0) {
            return "success";
        } else {
            return "fail";
        }
    }
//    实现了updateUserInfo方法，根据传入的用户名和旧密码查询用户信息，如果查询结果为空，则返回"the user name or password is incorrect"字符串；
//    否则调用UserFunctionDao中的updateUserInfo方法更新用户密码和手机号，并根据操作结果返回相应的处理结果。如果更新成功，则返回"success"字符串；否则返回"fail"字符串。
}


