package com.example.ordering.service;

import com.github.pagehelper.PageInfo;
import com.example.ordering.entity.TbFood;

import java.util.List;

/**
 * (TbFood)表服务接口
 *
 * @author makejava
 * @since 2023-05-19 20:49:20
 */
public interface TbFoodService {

    PageInfo<TbFood> selectByPage(int pageNum, int pageSize);
    List<TbFood> queryByConditions(TbFood tbFood);


}
