package com.example.ordering.service.impl;

import com.example.ordering.dao.FoodDao;
import com.example.ordering.entity.Food;
import com.example.ordering.service.FoodService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Food)表服务实现类
 *
 * @author makejava
 * @since 2023-05-24 17:16:48
 */
@Service("foodService")
public class FoodServiceImpl implements FoodService {
    @Resource
    private FoodDao foodDao;


    @Override
    public List<Food> findAll() {
        return foodDao.findAll();
    }





    /**
     * 新增数据
     *
     * @param food 实例对象
     * @return 实例对象
     */
    @Override
    public Food insert(Food food) {
        this.foodDao.insert(food);
        return food;
    }


    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.foodDao.deleteById(id) > 0;
    }
}
