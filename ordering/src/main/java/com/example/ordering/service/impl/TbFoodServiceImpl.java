package com.example.ordering.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.example.ordering.dao.TbFoodDao;
import com.example.ordering.entity.TbFood;
import com.example.ordering.service.TbFoodService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (TbFood)表服务实现类
 *
 * @author makejava
 * @since 2023-05-19 20:49:21
 */
@Service("tbFoodService")
public class TbFoodServiceImpl implements TbFoodService {
    @Resource
    private TbFoodDao tbFoodDao;

    public PageInfo<TbFood> selectByPage(int pageNum, int pageSize) {
        // 开始分页
        PageHelper.startPage(pageNum, pageSize);
        // 执行查询
        List<TbFood> userList = tbFoodDao.selectByPage();
        // 封装分页结果
        return new PageInfo<>(userList);
    }

    @Override
    public List<TbFood> queryByConditions(TbFood tbFood) {
        return tbFoodDao.queryByConditions(tbFood);
    }


}
