package com.example.ordering.service.impl;

import com.example.ordering.dao.OrderDao;
import com.example.ordering.entity.Order;
import com.example.ordering.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service/*标识该类是一个服务实现类，并自动装配OrderDao对象*/
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDao orderDao;

    @Override
    public List<Order> findByConditions1(String status) {
        return orderDao.findByConditions1(status);
    }
//    在findByConditions1()方法中，调用orderDao.findByConditions1(status)方法进行条件查询，并返回结果列表
}
