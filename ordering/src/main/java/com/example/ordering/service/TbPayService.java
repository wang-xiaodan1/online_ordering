package com.example.ordering.service;
import com.example.ordering.entity.Food;
import com.example.ordering.entity.TbFood;
import com.example.ordering.entity.TbPay;
import com.github.pagehelper.PageInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.math.BigDecimal;
import java.util.List;
/**
 * (TbPay)表服务接口
 *
 * @author makejava
 * @since 2023-05-26 19:22:33
 */
public interface TbPayService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    TbPay queryById(Integer id);
    BigDecimal selectTotalPrice();
    BigDecimal getTotalPrice();

    void insert(Object[] params);


    BigDecimal selectId(String orderAdress,String  orderTelephone,String  orderName);

    void updateStatus(BigDecimal id);

    PageInfo<TbPay> selectByPage(int pageNum, int pageSize);

    PageInfo<TbPay> selectByPagea(int pageNum, int pageSize);
}
