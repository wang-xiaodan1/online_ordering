package com.example.ordering.service;

import com.example.ordering.entity.Order;

import java.util.List;

public interface OrderService {
    List<Order> findByConditions1(String status);
//  这段代码是一个Java接口，用于定义订单服务的方法。它包含一个findByConditions1()方法，该方法接受一个status参数，并返回一个List<Order>类型的结果。
}

