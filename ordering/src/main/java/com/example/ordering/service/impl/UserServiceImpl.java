package com.example.ordering.service.impl;

import com.example.ordering.dao.UserDao;
import com.example.ordering.entity.Business1;
import com.example.ordering.entity.User1;
import com.example.ordering.entity.User2;
import com.example.ordering.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service


public  class UserServiceImpl implements UserService {

//将 UserDao 对象注入到 UserServiceImpl 中
    @Qualifier("userDao")
    @Autowired
    public UserDao UserDao;

    @Override

//根据用户名从数据库中查询对应的用户信息
    public User1 getUserByUsername(String UserName) {


        return UserDao.getUserByUsername(UserName);


    }
    //根据商家名从数据库中查询对应商家的信息
    public Business1 getUserByUserName1(String UserName1) {

        return UserDao.getUserByUsername1(UserName1);
    }




    @Autowired


    private UserDao userDao;


    @Override

//将用户注册信息插入到数据库中，包括电话号码、用户名和密码等信息
    public void insertUser(String phone1, String name1, String pass_word, String pass_word1) {


// 判断输入的密码是否一致


        if (!pass_word.equals(pass_word1)) {

//如果不一致，则抛出异常信息
            throw new RuntimeException("两次输入的密码不一致");


        }



//如果一致，则创建一个 User2 对象，并将传入的电话，用户名和密码等信息设置到 User2 对象中
        User2 user2 = new User2();


        user2.setPhone1(phone1);


        user2.setName1(name1);


        user2.setPass_word(pass_word);


        userDao.insertUser(user2);


    }
//将商家注册信息插入到数据库中
    @Override
    public void insertBusiness(String ShopName, String name, String Phone, String Email, String password, String Password2) {
        // 判断密码是否一致


        if (!password.equals(Password2)) {

//如果不一致，则抛出异常信息
            throw new RuntimeException("两次输入的密码不一致");


        }




//如果一致，则创建一个 User2 对象，并将传入的商家名和密码等信息设置到 User2 对象中
        Business1 business = new Business1( ShopName,  Phone,  name,  password,  "未审批");



//调用 UserDao 的 insertUser 方法将 User2 对象插入到数据库中
        userDao.insertBusiness(business);


    }

    /**
     * 查询商家入驻申请列表的service方法
     * @param ShopName
     * @param name
     * @param Phone
     * @return
     */
    @Override
    public List<Business1> queryBusinessList(int pageNum, int pageSize, String ShopName, String name, String Phone, String state){

        Integer start = (pageNum-1)*pageSize;
        Integer end = pageNum*pageSize;

        List<Business1> businessList = UserDao.queryBusinessList(ShopName,name,Phone,state,start,end);
        return businessList;
    }


    @Override
    public void updateBusiness(String Phone, String state){


        UserDao.updateBusinessInfo(Phone,state);
    }

}