package com.example.ordering.service;

//用于定义用户信息相关的服务方法
public interface UserFunctionService {
    String updateUserInfo(String username, String oldPassword, String newPassword, String phone);
}
//该方法需要传入用户名、旧密码、新密码和新手机号等参数，通过调用UserFunctionDao中的updateUserInfo方法进行数据库操作，并根据操作结果返回相应的处理结果