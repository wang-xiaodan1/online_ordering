package com.example.ordering.service.impl;

import com.example.ordering.dao.TbPayDao;
import com.example.ordering.entity.TbPay;
import com.example.ordering.service.TbPayService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * (TbPay)表服务实现类
 *
 * @author makejava
 * @since 2023-05-26 19:22:33
 */
@Service("tbPayService")
public class TbPayServiceImpl implements TbPayService {
    @Resource
    private TbPayDao tbPayDao;

    @Override
    public BigDecimal selectTotalPrice() {
        return tbPayDao.selectTotalPrice();
    }
    /**
     * 查询订单id
     *
     * @param selectParams 查询参数数组
     * @return 订单id
     */
    @Override
    public BigDecimal selectId(String orderAdress,String  orderTelephone,String  orderName) {
        return tbPayDao.selectId(orderAdress,orderTelephone,orderName);
    }
    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public TbPay queryById(Integer id) {
        return this.tbPayDao.queryById(id);
    }

    @Override
    public void insert(Object[] params) {
        Map<String, Object> map = new HashMap<>();
        map.put("orderAdress", params[0]);
        map.put("orderTelephone", params[1]);
        map.put("orderName", params[2]);
        map.put("totalPrice", params[3]);
        tbPayDao.insert(map);
    }
    /**
     * 分页查询
     *
     * @param tbPay 筛选条件
     * @param pageRequest      分页对象
     * @return 查询结果
     */

    /**
     * 新增数据
     *
     * @param tbPay 实例对象
     * @return 实例对象
     */
    @Override
    public void updateStatus(BigDecimal id) {
        tbPayDao.updateStatus(id);
    }
    /**
     * 修改数据
     *
     * @param tbPay 实例对象
     * @return 实例对象
     */
    public PageInfo<TbPay> selectByPage(int pageNum, int pageSize) {
        // 开始分页
        PageHelper.startPage(pageNum, pageSize);
        // 执行查询
        List<TbPay> userList = tbPayDao.selectByPage();
        // 封装分页结果
        return new PageInfo<>(userList);
    }


    public PageInfo<TbPay> selectByPagea(int pageNum, int pageSize) {
        // 开始分页
        PageHelper.startPage(pageNum, pageSize);
        // 执行查询
        List<TbPay> userList = tbPayDao.selectByPagea();
        // 封装分页结果
        return new PageInfo<>(userList);
    }

    public BigDecimal getTotalPrice() {
        List<TbPay> list = tbPayDao.selectAll();
        BigDecimal sum = BigDecimal.ZERO;
        for (TbPay ignored : list) {
            sum = sum.add(TbPay.getTotalPrice()
            );
        }
        return sum;

    }
}
