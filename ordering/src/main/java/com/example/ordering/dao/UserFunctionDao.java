package com.example.ordering.dao;

import com.example.ordering.entity.User1;
import org.apache.ibatis.annotations.Param;

//用于定义用户信息相关的数据库操作方法
public interface UserFunctionDao {
    User1 getUserByUsernameAndPassword(@Param("username") String username, @Param("password") String password);
    //    @Param注解将username和password参数映射到SQL语句中的同名参数
    int updateUserInfo(@Param("username") String username, @Param("newPassword") String newPassword, @Param("phone") String phone);
}







