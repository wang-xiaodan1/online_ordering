package com.example.ordering.dao;

import com.example.ordering.entity.Food;
import com.example.ordering.entity.TbFood;
import com.example.ordering.entity.TbPay;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * (TbPay)表数据库访问层
 *
 * @author makejava
 * @since 2023-05-26 19:22:33
 */
public interface TbPayDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    TbPay queryById(Integer id);
    BigDecimal selectTotalPrice();
    int updateStatus();
    List<TbPay> selectByPage();
    List<TbPay> selectByPagea();
    BigDecimal getTotalPrice();
    List<TbPay> selectAll();

    int insert(Map<String, Object> params);
    /**
     * 查询指定行数据
     *
     * @param tbPay 查询条件
     * @param pageable         分页对象
     * @return 对象列表
     */
    List<TbPay> queryAllByLimit(TbPay tbPay, @Param("pageable") Pageable pageable);

    /**
     * 统计总行数
     *
     * @param tbPay 查询条件
     * @return 总行数
     */
    long count(TbPay tbPay);

    /**
     * 新增数据
     *
     * @param tbPay 实例对象
     * @return 影响行数
     */

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<TbPay> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<TbPay> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<TbPay> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<TbPay> entities);

    /**
     * 修改数据
     *
     * @param tbPay 实例对象
     * @return 影响行数
     */

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);


    void updateStatus(BigDecimal id);

    BigDecimal selectId(@Param("orderAdress") String orderAdress,@Param("orderTelephone") String  orderTelephone,@Param("orderName") String  orderName);



    /**
     * 查询商家售卖总额
     * @param merchantName 商家名称
     * @return 售卖总额
     */

}

