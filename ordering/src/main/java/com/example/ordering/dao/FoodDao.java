package com.example.ordering.dao;

import com.example.ordering.entity.Food;

import java.util.List;

/**
 * (Food)表数据库访问层
 *
 * @author makejava
 * @since 2023-05-24 17:16:48
 */
public interface FoodDao {



    /**
     * 新增数据
     *
     * @param food 实例对象
     * @return 影响行数
     */
    int insert(Food food);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<Food> 实例对象列表
     * @return 影响行数
     */

    int deleteById(Integer id);

    List<Food> findAll();
}

