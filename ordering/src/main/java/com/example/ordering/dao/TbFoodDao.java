package com.example.ordering.dao;

import com.example.ordering.entity.Food;
import com.example.ordering.entity.TbFood;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * (TbFood)表数据库访问层
 *
 * @author makejava
 * @since 2023-05-19 20:49:19
 */
public interface TbFoodDao {

    List<TbFood> selectByPage();
    List<TbFood> queryByConditions(TbFood tbFood);


}

