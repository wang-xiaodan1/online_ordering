package com.example.ordering.dao;

import com.example.ordering.entity.Order;

import java.util.List;

public interface OrderDao {
    List<Order> findByConditions1(String status);
}
//定义了一个方法findByConditions1，该方法返回一个List<Order>类型的对象，


