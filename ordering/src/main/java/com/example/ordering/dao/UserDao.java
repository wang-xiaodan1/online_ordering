package com.example.ordering.dao;


import com.example.ordering.entity.Business1;
import com.example.ordering.entity.User1;
import com.example.ordering.entity.User2;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface UserDao {

//根据传入的参数username，在users表中查询符合条件的用户记录，并返回一个User1对象
    @Select("SELECT * FROM users WHERE username = #{username}")
    User1 getUserByUsername(String UserName);

    @Select("SELECT * FROM business WHERE name = #{name}")
    Business1 getUserByUsername1(String UserName1);

    void insertUser(User2 user2);

    void insertBusiness(Business1 business);


    /**
     * 查询商家申请列表
     * @param
     * @return
     */
    //@Select("SELECT * FROM business ")
    List<Business1> queryBusinessList (@Param("ShopName") String ShopName,
                                       @Param("name") String name,
                                       @Param("phone") String phone,
                                       @Param("state") String state,
                                       @Param("pageNum") Integer pageNum,
                                       @Param("pageSize") Integer pageSize);


    Integer updateBusinessInfo (@Param("phone") String phone,
                                       @Param("state") String state);

}
