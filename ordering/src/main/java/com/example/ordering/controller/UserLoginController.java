package com.example.ordering.controller;


import com.example.ordering.entity.User1;
import com.example.ordering.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller

public class UserLoginController {


    @Autowired


    private UserService userService;

    //当用户以 Post 方法请求 "/UserLogin" 时，进入 login 方法进行处理
    @RequestMapping(value = "UserLogin", method = RequestMethod.POST)

//获取数据
    public Object login(@RequestParam("UserName") String UserName, @RequestParam("Password") String Password) {

//管理员登录的用户名和密码
        if ("admin".equalsIgnoreCase(UserName) && "123456".equalsIgnoreCase(Password)) {
            return "BusinessList.html";

        }

        User1 user1 = userService.getUserByUsername(UserName);


        if (user1 == null) {
            //如果用户名不存在，进行提示
           return "message1.html";

        }
        else if (!user1.getPassword().equals(Password)) {
            //如果密码错误，显示找回密码功能按钮
            return "button.html";
        } else {
            // 登录成功，进入菜单浏览页面
           return "menu.html";
        }




    }
}
