package com.example.ordering.controller;

import com.example.ordering.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller


public class UserSignUpController {


    @Autowired


    private UserService userService;

    //当用户以 Post 方法请求 "/UserSignUp" 时，进入 userSignUp 方法进行处理
    @RequestMapping(value = "/UserSignUp", method = RequestMethod.POST)



//获取参数的值
    public String userSignUp(@RequestParam("phone1") String phone1,


                             @RequestParam("name1") String name1,


                             @RequestParam("pass_word") String pass_word,


                             @RequestParam("pass_word1") String pass_word1) {


// 调用Service层的方法将数据插入到数据库中


        userService.insertUser(phone1, name1, pass_word, pass_word1);


// 返回成功信息
        return "redirect:/page/message2.html";


    }

}
