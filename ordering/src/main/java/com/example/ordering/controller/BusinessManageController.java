package com.example.ordering.controller;

import com.example.ordering.entity.Business1;
import com.example.ordering.entity.User2;
import com.example.ordering.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 商户管理
 */
@Controller
@RequestMapping("/business")
public class BusinessManageController {

    @Autowired
    private UserService userService;


    /**
     * 动态查询商户信息列表
     *
     * @param ShopName
     * @param name
     * @param Phone
     * @return
     */
    @RequestMapping(value = "/queryBusinessList", method = RequestMethod.POST)
    public String queryBusinessList(@RequestParam(defaultValue = "1") int pageNum,
                                    @RequestParam(defaultValue = "10") int pageSize,@RequestParam("ShopName") String ShopName,
                                    @RequestParam("name") String name,
                                    @RequestParam("Phone") String Phone,@RequestParam("state") String state, Model model) {

        // 调用Service层的方法,根据传参查询入驻商家列表
        List<Business1> businessList = userService.queryBusinessList(pageNum,pageSize,ShopName, name, Phone, state);
        // 返回成功信息
        model.addAttribute("businessList",businessList);
        return "BusinessList";

    }

    /**
     * 控制商家状态  ：审批通过 、 封禁
     *
     * @param Phone
     * @return
     */
    @RequestMapping(value = "/updateBusiness", method = RequestMethod.POST)
    public String queryBusinessList(@RequestParam("phone") String Phone,@RequestParam("state") String state, Model model) {

        // 调用Service层的方法,根据传参查询入驻商家列表
        userService.updateBusiness(Phone, state);
        // 返回成功信息
        List<Business1> businessList = userService.queryBusinessList(1,10,null, null, null, null);
        // 返回成功信息
        model.addAttribute("businessList",businessList);
        return "BusinessList";
    }
}
