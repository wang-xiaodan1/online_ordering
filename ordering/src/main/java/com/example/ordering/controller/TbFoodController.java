package com.example.ordering.controller;

import com.github.pagehelper.PageInfo;
import com.example.ordering.entity.TbFood;
import com.example.ordering.service.TbFoodService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * (TbFood)表控制层
 *
 * @author makejava
 * @since 2023-05-19 20:49:17
 */
@Controller
@RequestMapping("/ordering")
public class TbFoodController {
    private static final Logger logger = LogManager.getLogger(TbFoodController.class);

    /**
     * 服务对象
     */
    @Resource
    private TbFoodService service;

    @RequestMapping(value = "/byConditions", method = RequestMethod.POST)
    public String getFoodsByConditions(TbFood tbFood, Model model) {
        List<TbFood> foods = service.queryByConditions(tbFood);
        model.addAttribute("foods", foods);
        return "menuQuery";
    }



    /**
     * 返回标准响应数据对象 其中包含分页数据
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping("/byPage")
    @ResponseBody
    public ResponseEntity<PageInfo<TbFood>> selectByPage(@RequestParam(defaultValue = "1") int pageNum,
                                                         @RequestParam(defaultValue = "10") int pageSize) {
        logger.info("selectByPage called with pageNum={}, pageSize={}", pageNum, pageSize);
        PageInfo<TbFood> pageInfo = service.selectByPage(pageNum, pageSize);
        return ResponseEntity.ok(pageInfo);
    }


}

