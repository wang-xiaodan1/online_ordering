package com.example.ordering.controller;
import com.example.ordering.entity.TbPay;
import com.example.ordering.entity.UserInfo;
import com.example.ordering.service.TbPayService;
import com.github.pagehelper.PageInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * (TbPay)表控制层
 *
 * @author makejava
 * @since 2023-05-26 19:22:33
 */
@RestController
@RequestMapping("tbPay")
public class TbPayController {
    private static final Logger logger = LogManager.getLogger(TbPayController.class);
    /**
     * 服务对象
     */
    @Resource
    private TbPayService tbPayService;
    @PostMapping("/update")
    @ResponseBody
    public void update(@RequestBody Map<String, String> jsonData) {
        String orderAdress = jsonData.get("orderAdress");
        String orderTelephone = jsonData.get("orderTelephone");
        String orderName = jsonData.get("orderName");
        BigDecimal id = tbPayService.selectId(orderAdress, orderTelephone, orderName);
        // 更新数据库中的订单状态
        tbPayService.updateStatus(id);
    }
    @PostMapping("/add")
    @ResponseBody
    public void pay(@RequestBody Map<String, String> params) {
        String orderAdress = params.get("orderAdress");
        String orderTelephone = params.get("orderTelephone");
        String orderName = params.get("orderName");
        BigDecimal totalPrice = tbPayService.selectTotalPrice();
        Object[] insertParams = {orderAdress, orderTelephone, orderName, totalPrice};
        tbPayService.insert(insertParams);
    }

    /**
     * 分页查询
     *
     * @param tbPay 筛选条件
     * @param pageRequest      分页对象
     * @return 查询结果
     */

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public ResponseEntity<TbPay> queryById(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(this.tbPayService.queryById(id));
    }

    @RequestMapping("/byPage")
    @ResponseBody
    public ResponseEntity<PageInfo<TbPay>> selectByPage(@RequestParam(defaultValue = "1") int pageNum,
                                                        @RequestParam(defaultValue = "10") int pageSize) {
        logger.info("selectByPage called with pageNum={}, pageSize={}", pageNum, pageSize);
        PageInfo<TbPay> pageInfo = tbPayService.selectByPage(pageNum, pageSize);
        return ResponseEntity.ok(pageInfo);
    }


    @RequestMapping("/byPagea")
    @ResponseBody
    public ResponseEntity<PageInfo<TbPay>> selectByPagea(@RequestParam(defaultValue = "1") int pageNum,
                                                        @RequestParam(defaultValue = "10") int pageSize) {
        logger.info("selectByPage called with pageNum={}, pageSize={}", pageNum, pageSize);
        PageInfo<TbPay> pageInfo = tbPayService.selectByPagea(pageNum, pageSize);
        return ResponseEntity.ok(pageInfo);
    }

@RequestMapping("/totalPrice")
public ModelAndView getTotalPrice() {
    BigDecimal totalPrice = tbPayService.getTotalPrice();
    ModelAndView modelAndView = new ModelAndView("Viewtotalsales");
    modelAndView.addObject("totalPrice", totalPrice);
    return modelAndView;
}
}

