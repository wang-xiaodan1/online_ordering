package com.example.ordering.controller;

import com.example.ordering.entity.Order;
import com.example.ordering.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/ordering")
public class OrderController {

    @Autowired
    private OrderService orderService;
//    在控制器中可以调用OrderService中的方法。


//    @RequestParam注解用于从请求参数中获取status的值，并将其传递给findByConditions1()方法进行条件查询。
    @PostMapping("/byConditions1")
    public String  findByConditions1(@RequestParam(value = "status", required = false) String status, Model model) {
        List<Order> orders = orderService.findByConditions1(status);
        model.addAttribute("orderings",orders);
        return "orderingQuery";
//   Model对象用于向视图层传递数据，将查询结果orders添加到model中。最后，返回视图名"orderingQuery"，用于展示查询结果。



    }
}

