package com.example.ordering.controller;
import com.example.ordering.entity.Food;
import com.example.ordering.entity.TbPay;
import com.example.ordering.service.FoodService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.github.pagehelper.PageInfo;
import javax.annotation.Resource;
import java.util.List;
import java.math.BigDecimal;


/**
 * (Food)表控制层
 *
 * @author makejava
 * @since 2023-05-24 17:16:48
 */
@RestController
@RequestMapping("/food")
public class FoodController {
    /**
     * 服务对象
     */
    @Resource
    private FoodService foodService;

    /**
     * 新增数据
     *
     * @param food 实体
     * @return 新增结果
     */
    @RequestMapping("/add")
    @ResponseBody
    public ResponseEntity<Food> add(@RequestBody Food food) {
        return ResponseEntity.ok(this.foodService.insert(food));
    }

    @GetMapping("/foods")
    public List<Food> list() {
        return foodService.findAll(); // 从数据库中获取所有菜品数据

    }
    @DeleteMapping("/delete/{id}")
    public boolean delete(@PathVariable("id") Integer id) {
        return foodService.deleteById(id);
    }
}

