package com.example.ordering.controller;

import com.example.ordering.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class BusinessSignUpController {
    @Autowired


    private UserService userService;

    //当用户以 Post 方法请求 "/BusinessSignUp" 时，进入businessSignUp方法进行处理
    @RequestMapping(value = "/BusinessSignUp", method = RequestMethod.POST)



//获取参数的值
    public String businessSignUp(@RequestParam("ShopName") String ShopName,


                             @RequestParam("name") String name,

                             @RequestParam("Phone") String Phone,

                             @RequestParam("Email") String Email,


                             @RequestParam("password") String password,


                             @RequestParam("Password2") String Password2) {


// 调用Service层的方法将数据插入到数据库中


        userService.insertBusiness(ShopName,name, Phone,Email,password,Password2);


// 返回成功信息
        return "redirect:/page/message4.html";


    }
}
