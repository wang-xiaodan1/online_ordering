package com.example.ordering.controller;


import com.example.ordering.entity.UserInfo;
import com.example.ordering.service.TbPayService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;

@Controller
public class OrderInfoController {

    @Resource
    private TbPayService tbPayService;

    @GetMapping("/ordering")
    public String order() {
        return "ordering";
    }

    @RequestMapping("/pay")
    public void pay(@RequestBody UserInfo userInfo, Model model, HttpSession session) {

        BigDecimal id = tbPayService.selectId(userInfo.getOrderAdress(), userInfo.getOrderTelephone(), userInfo.getOrderName());
        // 更新数据库中的订单状态
        tbPayService.updateStatus(id);
        System.out.println(userInfo);
        session.setAttribute("userInfo",userInfo);
    }
}
