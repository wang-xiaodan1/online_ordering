package com.example.ordering.controller;

import com.example.ordering.service.UserFunctionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UserFunctionController {

    @Autowired
    private UserFunctionService userFunctionService;

//   定义了一个@RequestMapping注解的updateUserInfo方法，用于处理"/updateUserInfo"接口的POST请求，
    @RequestMapping("/updateUserInfo")
    @ResponseBody
    public String updateUserInfo(@RequestParam String username, @RequestParam String oldPassword,
                                 @RequestParam String newPassword, @RequestParam String phone) {
        return userFunctionService.updateUserInfo(username, oldPassword, newPassword, phone);
    }
//  并通过@ResponseBody注解将返回值转换为字符串类型的响应结果。该方法中使用@RequestParam注解获取请求参数中的username、oldPassword、newPassword和phone四个参数，
//  并调用userFunctionService中的updateUserInfo方法进行处理，并将处理结果作为响应结果返回。


}

