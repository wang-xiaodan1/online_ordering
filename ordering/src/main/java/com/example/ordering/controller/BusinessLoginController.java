package com.example.ordering.controller;

import com.example.ordering.entity.Business1;
import com.example.ordering.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class BusinessLoginController {
    @Autowired
    private UserService userService;

//当用户以 Post 方法请求 "/BusinessLogin" 时，进入 login 方法进行处理
    @RequestMapping(value = "/BusinessLogin", method = RequestMethod.POST)

    public Object login(@RequestParam("UserName1") String UserName1, @RequestParam("Password1") String Password1) {
// 获取参数值

        Business1 business1 = userService.getUserByUserName1(UserName1);

//查询数据库中是否存在与输入用户名匹配的商家
        if (business1 == null) {
            //如果商家名不存在，则跳转到小提示，去选择继续登录或重新注册
            return "message3.html";

        }
        // 商户处于封禁状态时，提示无法登录
        if ("未审批".equals(business1.getState()) || "封禁".equals(business1.getState())) {
            return "message5.html";
        }
        else if (!business1.getPassword().equals(Password1)) {
            //商家名存在但密码错误，则提示找回密码按钮
            return "button.html";
        } else {
            // 登录成功，跳转到商家后台管理页面
            return "Sjmenu.html";
        }




    }
}

