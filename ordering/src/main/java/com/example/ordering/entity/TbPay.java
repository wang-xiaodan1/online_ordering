package com.example.ordering.entity;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * (TbPay)实体类
 *
 * @author makejava
 * @since 2023-05-26 19:22:33
 */
public class TbPay implements Serializable {
    private static final long serialVersionUID = -92922235775545869L;
    
    private Integer id;
    /**
     * 配送地址
     */
    private String orderAdress;
    /**
     * 电话号码
     */
    private String orderTelephone;
    /**
     * 顾客姓名
     */
    private String orderName;
    /**
     * 总价
     */
    private static BigDecimal totalPrice;
    /**
     * 状态
     */
    private String status;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderAdress() {
        return orderAdress;
    }

    public void setOrderAdress(String orderAdress) {
        this.orderAdress = orderAdress;
    }

    public String getOrderTelephone() {
        return orderTelephone;
    }

    public void setOrderTelephone(String orderTelephone) {
        this.orderTelephone = orderTelephone;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public static BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}

