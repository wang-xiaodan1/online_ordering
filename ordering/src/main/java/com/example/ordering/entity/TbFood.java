package com.example.ordering.entity;

import java.io.Serializable;

/**
 * (TbFood)实体类
 *
 * @author makejava
 * @since 2023-05-19 20:49:20
 */
public class TbFood implements Serializable {
    private static final long serialVersionUID = 296910086741433578L;
    
    private Integer id;
    /**
     * 菜品类型
     */
    private String foodtypeId;
    /**
     * 菜品名称
     */
    private String foodName;
    /**
     * 价格¥
     */
    private String price;
    /**
     * 商家名称
     */
    private String merchantName;
    /**
     * 总价¥
     */
    private String totalPrice;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFoodtypeId() {
        return foodtypeId;
    }

    public void setFoodtypeId(String foodtypeId) {
        this.foodtypeId = foodtypeId;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

}

