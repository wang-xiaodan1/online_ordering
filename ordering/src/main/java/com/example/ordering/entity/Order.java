package com.example.ordering.entity;

import java.math.BigDecimal;

public class Order {
    private Long id;
    private String order_adress;
    private String order_telephone;
    private String order_name;
    private BigDecimal total_price;
    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrder_adress() {
        return order_adress;
    }

    public void setOrder_adress(String order_adress) {
        this.order_adress = order_adress;
    }

    public String getOrder_telephone() {
        return order_telephone;
    }

    public void setOrder_telephone(String order_telephone) {
        this.order_telephone = order_telephone;
    }

    public String getOrder_name() {
        return order_name;
    }

    public void setOrder_name(String order_name) {
        this.order_name = order_name;
    }

    public BigDecimal getTotal_price() {
        return total_price;
    }

    public void setTotal_price(BigDecimal total_price) {
        this.total_price = total_price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
