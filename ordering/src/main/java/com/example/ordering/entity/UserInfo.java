package com.example.ordering.entity;

public class UserInfo {
    /**
     * 配送地址
     */
    private String orderAdress;
    /**
     * 电话号码
     */
    private String orderTelephone;
    /**
     * 顾客姓名
     */
    private String orderName;

    public String getOrderAdress() {
        return orderAdress;
    }

    public void setOrderAdress(String orderAdress) {
        this.orderAdress = orderAdress;
    }

    public String getOrderTelephone() {
        return orderTelephone;
    }

    public void setOrderTelephone(String orderTelephone) {
        this.orderTelephone = orderTelephone;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public UserInfo(String orderAdress, String orderTelephone, String orderName) {
        this.orderAdress = orderAdress;
        this.orderTelephone = orderTelephone;
        this.orderName = orderName;
    }

    public UserInfo() {
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "orderAdress='" + orderAdress + '\'' +
                ", orderTelephone='" + orderTelephone + '\'' +
                ", orderName='" + orderName + '\'' +
                '}';
    }
}
