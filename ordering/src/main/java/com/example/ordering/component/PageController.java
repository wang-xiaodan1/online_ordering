package com.example.ordering.component;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 统一页面跳转管理
 * @author yaojianfeng
 */
@Controller
@RequestMapping("/page")
public class PageController {


    @RequestMapping("/menu")
    public String menu(){
        return "menu";
    }

    @RequestMapping("/menuQuery")
    public String menuQuery(){
        return "menuQuery";
    }
    @RequestMapping("/Ordering")
    public String Ordering(){
        return "Ordering";
    }
    @RequestMapping("/pay")
    public String pay(){
        return "pay";
    }
    @RequestMapping("/orderingQuery")
    public String orderingQuery(){
        return "orderingQuery";
    }
    @RequestMapping("/userFunction")
    public String userFunction(){
        return "userFunction";
    }
    @RequestMapping("/Viewtotalsales")
    public String Viewtotalsales(){
        return "Viewtotalsales";
    }

}
