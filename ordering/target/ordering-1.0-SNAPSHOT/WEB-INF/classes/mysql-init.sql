CREATE DATABASE IF NOT EXISTS ordering DEFAULT CHARACTER SET utf8;
USE ordering;
CREATE TABLE tb_food
(
    id int(11) NOT NULL auto_increment,
    foodType_id varchar(20) default NULL COMMENT '菜品类型',
    food_name varchar(50) default NULL COMMENT '菜品名称',
    price double default NULL COMMENT '价格¥',
    merchant_name varchar(100) default NULL COMMENT '商家名称',
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE food
(
    id int(11) NOT NULL auto_increment,
    foodType_id varchar(20) default NULL COMMENT '菜品类型',
    food_name varchar(50) default NULL COMMENT '菜品名称',
    price double default NULL COMMENT '价格¥',
    merchant_name varchar(100) default NULL COMMENT '商家名称',
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE tb_pay
(
    id int(11) NOT NULL auto_increment,
    order_adress varchar(300) default NULL COMMENT '配送地址',
    order_telephone varchar(100)  default NULL COMMENT '电话号码',
    order_name varchar(200) default NULL COMMENT '顾客姓名',
    total_price double default NULL COMMENT '总价',
    status varchar(20) default null comment '状态',
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `approval`;
CREATE TABLE `approval`  (
                             `id` int(11) NOT NULL AUTO_INCREMENT,
                             `store_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                             `person` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                             `numbers` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                             `mailbox` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                             `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                             `states` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                             `disables` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


INSERT INTO tb_food VALUES(1,'早餐','包子',10,'天津狗不理包子'),(2,'早餐','豆浆',3,'永和豆浆'),(3,'早餐','油条',1,'早餐食铺子'),(4,'早餐','煎包',2,'早餐食铺子'),
                          (5,'早餐','煎饼',4,'早餐食铺子'), (6,'早餐','鸡蛋',1,'早餐食铺子'),(7,'早餐','玉米粥',2,'早餐粥铺子'),(8,'早餐','黑米粥',2,'早餐粥铺子'),(9,'早餐','八宝粥',3,'早餐粥铺子'),
                          (10,'早餐','胡辣汤',4,'早餐粥铺子'),(11,'中餐','过桥米线',15,'过桥米线'),(12,'中餐','黄焖鸡米饭',15,'鱼米之乡'),(13,'中餐','大米+素',9,'鱼米之乡'),(14,'中餐','大米+肉',12,'鱼米之乡'),
                          (15,'中餐','方便面',8,'面食铺子'),(16,'中餐','重庆小面',9,'面食铺子'),(17,'中餐','虾丸面',10,'面食铺子'),(18,'中餐','拌面',10,'面食铺子'),(19,'中餐','麻辣烫',10,'面食铺子'),
                          (20,'中餐','饺子',9,'面食铺子'),(21,'晚餐','玉米粥',2,'早餐粥铺子'),(22,'晚餐','黑米粥',2,'早餐粥铺子'),(23,'晚餐','八宝粥',3,'早餐粥铺子'),(24,'晚餐','汉堡包',9,'快餐食品铺'),
                          (25,'晚餐','凉皮',8,'濮阳凉皮'),(26,'晚餐','煎饼',4,'早餐食铺子'),(27,'晚餐','炒饭',11,'鱼米之乡'),(28,'晚餐','蒸蛋',6,'晚食铺子'),(29,'晚餐','炒饼',9,'晚食铺子'),(30,'晚餐','鸡汤',18,'晚食铺子'),
                          (31,'甜食','酸奶',10,'甜食铺子'),(32,'甜食','枣泥糕',10,'甜食铺子'),(33,'甜食','绿豆糕',10,'甜食铺子'),(34,'甜食','蛋糕',20,'甜食铺子'),(35,'甜食','提拉米苏',20,'甜食铺子'),
                          (36,'甜食','果冻',4,'甜食铺子'),(37,'甜食','枣糕',10,'甜食铺子'),(38,'甜食','甜番薯',10,'甜食铺子'),(39,'米','大米粥',2,'食米铺'),(40,'米','土豆牛肉饭',20,'食米铺'),(41,'米','参枣糯米饭',20,'食米铺'),
                          (42,'米','火腿黑米饭',20,'食米铺'),(43,'米','雪片糕',15,'食米铺'),(44,'米','糯米酒',30,'食米铺'),(45,'面','兰州拉面',15,'食面铺'),(46,'面','荞麦面',12,'食面铺'),(47,'面','红烧牛肉面',12,'食面铺'),
                          (48,'面','刀削面',10,'食面铺'),(49,'面','杂酱面',10,'食面铺'),(50,'面','阳春面',10,'食面铺'),(51,'面','烤饼',8,'食面铺'),
                          (52,'饮品','可乐',6,'饮品小铺'), (53,'饮品','雪碧',6,'饮品小铺'), (54,'饮品','果茶',8,'饮品小铺'), (55,'饮品','冰红茶',6,'饮品小铺'), (56,'饮品','利趣拿铁',10,'饮品小铺');

CREATE TABLE users  (
                        username varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
                        password varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,

                        phone varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO users VALUES ('Lily', 'Lily123', '12345');
INSERT INTO users VALUES ('Amy', '123456789', '234');
INSERT INTO users VALUES ('zhangsan', 'zhangsan', '31');
INSERT INTO users VALUES ('Liuyu', '15345', '23');
INSERT INTO users VALUES ('happy', '666666', '98765');


SET FOREIGN_KEY_CHECKS = 1;
select *from users;

CREATE TABLE business
(
  shopName varchar(20),
  phone varchar(20),
    name varchar(20),
    password varchar(50),
    state varchar(50)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into business values
('店铺1','15091888881','wangqiang','888888','未审批'),
('店铺2','15091888882','zhangwei','686868','未审批'),
('店铺3','15091888883','Liuyuan','868686','未审批'),
('店铺5','15091888885','duanmuli','666888','未审批')
;
select * from business;
